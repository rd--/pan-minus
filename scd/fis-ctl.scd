/* x=0 y=1 s=2 i=3 */
(var fd = NetAddr.new(hostname: "127.0.0.1",port: 57147)
;var rc = Rect.new(left: 150, top: Window.screenBounds.height - 150, width: 620, height: 100)
;var w = Window.new(name:"fis-ctl",bounds: rc, resizable: false).front
;var mk_ctl =
 {arg lbl, d1, d2, d3, ix, d4
 ;var sp = ControlSpec.new(minval: d1,maxval: d2,warp: 'lin',step: d3)
 ;EZSlider.new(parent: w,bounds: 600@20,label: lbl,controlSpec: sp,action:{arg z; fd.sendMsg("/c_set",ix,z.value)},initVal: d4)}
;w.view.decorator=FlowLayout(w.view.bounds)
;w.view.decorator.gap=2@2
;mk_ctl.("x",-5.0,5.0,0.0,0,-5.0)
;mk_ctl.("y",-5.0,5.0,0.0,1,-5.0)
;mk_ctl.("s",0.1,20.0,0.0,2,10.0)
;mk_ctl.("i",0,10,1,3,0.0))
