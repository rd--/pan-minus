pan-minus
---------

a simple non-optimised [pan](http://conal.net/Pan/) variant

Conal Elliott "Functional Image Synthesis", 2000
([PDF](http://conal.net/papers/bridges2001/bridges-medres.pdf))

![](sw/pan-minus/help/jpeg/fig-16.0.jpeg)

FIGURES 1-19: [HTML](?t=pan-minus&e=help/img.md)

tested-with:
[gcc](http://gcc.gnu.org/)-10.2.1
[clang](https://clang.llvm.org/)-11.0.1
[ghc](https://www.haskell.org/ghc/)-9.10.1

© [rohan drape](http://rohandrape.net),
  2005-2025,
  [gpl](http://gnu.org/copyleft/)
