## FIG 1-6

![](sw/pan-minus/help/jpeg/fig-1.0.jpeg)
![](sw/pan-minus/help/jpeg/fig-2.0.jpeg)
![](sw/pan-minus/help/jpeg/fig-3.0.jpeg)
![](sw/pan-minus/help/jpeg/fig-4.0.jpeg)
![](sw/pan-minus/help/jpeg/fig-5.0.jpeg)
![](sw/pan-minus/help/jpeg/fig-6.0.jpeg)

## FIG 7

![](sw/pan-minus/help/jpeg/fig-7.0.jpeg)
![](sw/pan-minus/help/jpeg/fig-7.17.jpeg)
![](sw/pan-minus/help/jpeg/fig-7.18.jpeg)
![](sw/pan-minus/help/jpeg/fig-7.2.jpeg)
![](sw/pan-minus/help/jpeg/fig-7.21.jpeg)
![](sw/pan-minus/help/jpeg/fig-7.24.jpeg)

## FIG 8-16

![](sw/pan-minus/help/jpeg/fig-8.0.jpeg)
![](sw/pan-minus/help/jpeg/fig-9.0.jpeg)
![](sw/pan-minus/help/jpeg/fig-11.0.jpeg)
![](sw/pan-minus/help/jpeg/fig-13.0.jpeg)
![](sw/pan-minus/help/jpeg/fig-15.0.jpeg)
![](sw/pan-minus/help/jpeg/fig-16.0.jpeg)

## FIG 17-19

![](sw/pan-minus/help/jpeg/fig-17.0.jpeg)
![](sw/pan-minus/help/jpeg/fig-18.0.jpeg)
![](sw/pan-minus/help/jpeg/fig-19.0.jpeg)
