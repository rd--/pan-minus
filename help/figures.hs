-- | Conal Elliott "Functional Image Synthesis", Microsoft Research, 2000

import Graphics.Pan.Minus {- pan-minus -}

main :: IO ()
main = do
  let wr :: Double -> ImageC -> IO ()
      wr n f = writeImageC (concat ["/tmp/fig-",show n,".ppm"]) 250 250 f
      fC n im = im . uscaleP (n/2)
      fR n im = bwIm (im . uscaleP (n/2))
      fCW n im = fC n (im `over` whiteI)
  wr 1 (fR 7 vstrip)
  wr 2 (fR 7 checker)
  wr 3 (fR 10 altRings)
  wr 4 (fR 10 (polarChecker 10))
  wr 5 (fC 10 (grey . wavDist))
  wr 6 (fC 2 bilerpBRBW)
  wr 7 (fC 7 (lerpI wavDist (bwIm (polarChecker 10)) (bwIm checker)))
  wr 8 (fC 10 ybRings)
  wr 9 (fR 3 udisk)
  wr 10 (fR 3 (udisk . uscaleP 2))
  wr 11 (fR 5 (swirl 1 vstrip))
  wr 13 (fR 2.5 (wedgeAnnulus 0.25 7))
  wr 15 (fR 10 (shiftXor 2.6 altRings))
  wr 16 (fR 7 (xorgon 8 (7/4) altRings))
  wr 17 (fR 2.2 (radInvert checker))
  wr 18 (fC 10 (rippleRadius 8 0.3 ybRings))
  wr 19 (fCW 15 (swirl 8 (rippleRadius 5 0.3 (cropRadius 5 ybRings))))
  wr 7.17 (fCW 2.5 (crop (wedgeAnnulus 0.25 7) ybRings))
  wr 7.18 (fCW 2.5 (swirl 1.25 (crop (wedgeAnnulus 0.25 7) ybRings)))
  wr 7.20 (fCW 15 (cropRadius 5 (rippleRadius 8 0.3 ybRings)))
  wr 7.21 (fCW 10 (swirl 8 (cropRadius 5 (rippleRadius 5 0.3 ybRings))))
  wr 7.24 (fCW 22 (circleLimit 10 (bwIm checker)))
