import Graphics.Pan.Minus {- pan-minus -}

main :: IO ()
main = do
  let wr :: Double -> ImageC -> IO ()
      wr n f = writeImageC ("frac-" ++ show n ++ ".ppm") 250 250 f
      fC n im = im . uscaleP n
      fR n im = bwIm (im . uscaleP n)
  wr 0.0 (fR 2 (mset_img_b 2 500))
  wr 0.1 (fC 2 (grey . mset_img_f 2 500))
