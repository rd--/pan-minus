/* mandelbrot quadratic recurrence equation z_(n+1)==z_n^2+C */
c32 mset(c32 z, c32 C)
{
  return c32_add(c32_square(z), C);
}

/* return 1 if C does not escape, else 0-1 normalized level */
f32 mset_level(c32 z0, c32 C, f32 rmax, int count)
{
  int n = 0;
  c32 z = mset(z0,C);
  while(1) {
    if(n==count) {
      return 1.0;
    } else {
      if(c32_mod(z) > rmax) {
	return (f32)n / (f32)count;
      } else {
	n += 1;
	z = mset(z,C);
      }
    }
  }
}

f32 mset_f32(c32 c) {
  return mset_level(c, c, 2.0, 40);
}

Color mset_grey(p2 *s, p2 p) {
  c32 c = {p.x - s->x, p.y - s->y};
  return grey(1.0 - mset_f32(c));
}

f32 jset_f32(c32 c, c32 z0) {
  return mset_level(z0, c, 2.0, 20);
}

Color jset_grey(p2 *s, p2 p) {
  c32 c = {s->x, s->y};
  c32 z0 = {p.x, p.y};
  return grey(1.0 - jset_f32(c, z0));
}
