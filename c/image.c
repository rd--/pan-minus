#define PI 3.14159265358979323846264338327950288

bool checker(p2 p)
{
  return (i32)(floorf(p.x) + floorf(p.y)) % 2;
}

bool udisk(p2 p)
{
  return p2_dot(p,p) < 1.0;
}

p2 polarChecker_sc(f32 n,p2 p)
{
  p2 r = {p.x / 10.0, p.y * (n / PI)};
  return r;
}

bool polarChecker(f32 n, p2 p)
{
  return checker(polarChecker_sc(n,p2_cartesian_to_polar(p)));
}

f32 wavDist(p2 p)
{
  return (1 + cosf(PI * p2_mag(p))) / 2.0;
}
