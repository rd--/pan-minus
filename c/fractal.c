/* cactus fractal: z_(n+1)==z_n^3+(z_0-1)z_n-z_0 */

c64 cactus(c64 z, c64 z0)
{
  c64 a = c64_cube(z);
  c64 b = c64_sub(z0, c64_one);
  c64 c = c64_mul(b, z);
  c64 d = c64_add(a,c);
  return c64_sub(d,z0);
}


/* the lattes function W1(z) */

c32 lattes(c32 z)
{
  c32 qC = {0.25, 0.0};
  c32 oC = {1.0, 0.0};
  c32 a = c32_add(oC, c32_mul(z, z));
  c32 b = c32_mul(a,a);
  c32 c = c32_mul(z, c32_sub(c32_mul(z, z), oC));
  return c32_mul(qC, c32_div(b, c));
}
