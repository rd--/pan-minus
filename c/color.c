/* r, g, b and a are all in [0,1] */

typedef struct { f32 b, g, r, a; } Color;

Color bgra(f32 b, f32 g, f32 r, f32 a)
{
  Color c = {b, g, r, a};
  return c;
}

Color bgr(f32 b, f32 g, f32 r)
{
  Color c = {b, g, r, 1.0};
  return c;
}

f32 colorB(Color c) { return c.b; }
f32 colorG(Color c) { return c.g; }
f32 colorR(Color c) { return c.r; }
f32 colorA(Color c) { return c.a; }

Color red       = {0, 0, 1, 1};
Color orange    = {0, 0.65, 1, 1};
Color green     = {0, 1, 0, 1};
Color yellow    = {0, 1, 1, 1};
Color blue      = {1, 0, 0, 1};
Color white     = {1, 1, 1, 1};
Color black     = {0, 0, 0, 1};
Color invisible = {0, 0, 0, 0};

f32 lerpC_h(f32 w, f32 x1, f32 x2)
{
  return (w * x1) + ((1 - w) * x2);
}

Color lerpC(f32 w, Color c1, Color c2)
{
  Color c = { lerpC_h(w, c1.b, c2.b),
	      lerpC_h(w, c1.g, c2.g),
	      lerpC_h(w, c1.r, c2.r),
	      lerpC_h(w, c1.a, c2.a) };
  return c;
}

Color grey   (f32 x)          { return bgr(x, x, x); }
Color lighten(f32 x, Color c) { return lerpC(x, c, white); }
Color darken (f32 x, Color c) { return lerpC(x, c, black); }

Color negC(Color c)
{
  Color r = {c.a - c.b, c.a - c.g, c.a - c.r, c.a};
  return r;
}

Color fadeC(f32 f, Color c)
{
  Color r = {c.b * f, c.g * f, c.r * f, c.a * f};
  return r;
}

f32 overC_h(f32 a1, f32 x1, f32 x2)
{
  return x1 + ((1 - a1) * x2);
}

Color overC(Color c1, Color c2)
{
  Color c = { overC_h(c1.a, c1.b, c2.b),
	      overC_h(c1.a, c1.g, c2.g),
	      overC_h(c1.a, c1.r, c2.r),
	      overC_h(c1.a, c1.a, c2.a) };
  return c;
}
