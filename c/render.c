typedef Color(*imgC_f)(void*,p2);
typedef bool(*imgR_f)(void*,p2);

/*
  d = data, w = width, h = height, panX = x value at left edge, panY =
  y value at lower edge, n = scalar, s = secret cookie, f = function
 */

void renderC(u8 *d, i32 w, i32 h, f32 panX, f32 panY, f32 n, void* s, imgC_f f)
{
  i32 i, j;
  f32 xi, yi;
  p2 p;
  Color c;

  xi = yi = n / w;
  p.y = n + panY;
  for(i=0; i<w; i++) {
    p.x = panX;
    for(j=0;j<h; j++) {
      c = f(s, p);
      p.x += xi;
      *d++ = (u8)floorf(c.r * 255.0);
      *d++ = (u8)floorf(c.g * 255.0);
      *d++ = (u8)floorf(c.b * 255.0);
    }
    p.y -= yi;
  }
}
