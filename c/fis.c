/***** fis.c *****/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <pthread.h>

/* r-common/c */
#include "r-common/c/byte-order.c"
#include "r-common/c/clip.c"
#include "r-common/c/complex.c"
#include "r-common/c/img.c"
#include "r-common/c/int.h"
#include "r-common/c/memory.c"
#include "r-common/c/network.c"
#include "r-common/c/osc.c"
#include "r-common/c/point.c"
#include "r-common/c/rand.c"
#include "r-common/c/taus88.c"
#include "r-common/c/time-current.c"
#include "r-common/c/time-timespec.c"
#include "r-common/c/time-timeval.c"
#include "r-common/c/vector.c"
#include "r-common/c/x11.c"
#include "r-common/c/ximg.c"

/* local */
#include "color.c"
#include "perlin.c"
#include "render.c"

#include "image.c"
#include "mset.c"

struct taus88 rng;

Color bwIm(bool b)
{
  return b ? black : white;
}

Color checker_bw(void *PTR, p2 p)
{
  return bwIm(checker(p));
}

Color gth_bw(void *PTR, p2 p)
{
  return bwIm(p.x > 0.5 && p.y > 0.5);
}

Color random_grey(void *PTR, p2 p)
{
  return grey(rand_f32(&rng, 0.0, 1.0));
}

Color wavDist_grey(void *PTR, p2 p)
{
  return grey(wavDist(p));
}

Color xgty_bw(void *PTR, p2 p)
{
  return bwIm(p.x > p.y);
}

Color gtp_bw(p2 *m, p2 p)
{
  return bwIm(p.x > m->x && p.y > m->y);
}

Color polarChecker_bw(p2 *m, p2 p)
{
  return bwIm(polarChecker(m->x, p));
}

Color wtq_bw(p2 *m, p2 p)
{
  return bwIm(p2_distance(p, *m) > 0.25);
}

Color wtqf_clr(p2 *m, p2 p)
{
  f32 d = p2_distance(p, *m);
  return (d > 0.25) ? white : lerpC(d*4,yellow,blue);
}

Color wtxy_bw(p2 *m, p2 p)
{
  f32 z = p2_distance(p, *m) * 4;
  return bwIm(z > p.x && z > p.y);
}

Color perlin_grey(p2 *s, p2 p)
{
  return grey(PerlinNoise2D(p.x,
			    p.y,
			    1.0 + clip_f32(s->x, 0.0, 1.0),
			    1.0 + clip_f32(s->y, 0.0, 1.0),
			    6));
}

typedef struct
{
  f32 ctl[512];
  i32 ctln;
  int fd;
} fis_t;

void *ctl_handler(void *PTR)
{
  fis_t *f = PTR;
  while(true) {
    const int packet_extent = 32;
    uint8_t packet[packet_extent];
    int packet_sz = xrecv(f->fd, packet, 32, 0);
    osc_data_t o[2] ;
    if(osc_parse_message("/c_set", ",if", packet, packet_sz, o)) {
      int i = o[0].i;
      if(i >= 0 && i < f->ctln) {
	f->ctl[i] = o[1].f;
      }
    } else {
      eprintf("fis: dropped packet: %8s\n", packet);
    }
  }
  return NULL ;
}

void
usage(void)
{
  printf("fis\n");
  printf(" -h INT   height (default=500)\n");
  printf(" -i INT   image function (0=jset, 1=mset, 2=perlin ...)\n");
  printf(" -o       osc control (in place of mouse control)\n");
  printf(" -p INT   udp port (default=57147)\n");
  printf(" -s FLOAT scalar (default=10.0)\n");
  printf(" -w INT   width (default=500)\n");
  printf(" -x FLOAT initial x (default=-5.0)\n");
  printf(" -y FLOAT initial y (default=-5.0)\n");
  printf(" -z FLOAT initial z (default=0.1)\n");
  printf("fis\n");
  exit(0);
}

enum ctl_mode_t {ctl_mouse, ctl_osc};

int
main(int argc, char *argv[])
{
  /* opt */
  int w = 500;
  int h = 500;
  f32 s = 10.0;
  f32 x = -5.0;
  f32 y = -5.0;
  f32 z = 0.1;
  int img_i = 0; /* -i */
  enum ctl_mode_t ctl_mode = ctl_mouse; /* -o */
  int port_n = 57147; /* -p */
  /* end opt */
  u8 *img = NULL;
  f64 frameCount = 0.0;
  pthread_t ctl_thread;
  fis_t fis;
  int img_k = 11;
  imgC_f img_f[img_k];
  img_f[0] = (imgC_f)jset_grey;
  img_f[1] = (imgC_f)mset_grey;
  img_f[2] = (imgC_f)perlin_grey;
  img_f[3] = (imgC_f)polarChecker_bw;
  img_f[4] = (imgC_f)wavDist_grey;
  img_f[5] = (imgC_f)checker_bw;
  img_f[6] = (imgC_f)random_grey;
  img_f[7] = (imgC_f)wtxy_bw;
  img_f[8] = (imgC_f)wtqf_clr;
  img_f[9] = (imgC_f)wtq_bw;
  img_f[10] = (imgC_f)gtp_bw;
  /* gth_bw xgty_bw */

  int c;
  while(( c = getopt(argc, argv, "h:i:op:s:w:x:y:z:")) != -1) {
    switch(c) {
    case 'h': h = (int)strtol(optarg, NULL, 0); break;
    case 'i': img_i = (int)strtol(optarg, NULL, 0); break;
    case 'o': ctl_mode = ctl_osc; break;
    case 'p': port_n = (int)strtol(optarg, NULL, 0); break;
    case 's': s = strtod(optarg, NULL); break;
    case 'w': w = (int)strtol(optarg, NULL, 0); break;
    case 'x': x = strtod(optarg, NULL); break;
    case 'y': y = strtod(optarg, NULL); break;
    case 'z': z = strtod(optarg, NULL); break;
    default: usage();
    }
  }

  if(img_i >= img_k) {
      die("img_i >= img_k");
  }

  printf("w=%d, h=%d, x=%f, y=%f, s=%f, z=%f, i=%d\n", w, h, x, y, s, z, img_i);

  Ximg_t *ximg = ximg_open(w, h, "ximg.help");
  img = malloc(w*h*3);
  memset(img,128,w*h*3);

  if (ctl_mode == ctl_osc) {
    fis.ctln = 512;
    fis.ctl[0] = x;
    fis.ctl[1] = y;
    fis.ctl[2] = s;
    fis.ctl[3] = (float)img_i;
    fis.fd = socket_udp(0);
    bind_inet(fis.fd, NULL, port_n);
    pthread_create(&ctl_thread, NULL, ctl_handler, &fis);
  }

  f64 start = current_time_as_utc_real();
  taus88_std_init(&rng);
  while(1) {
    p2 p;
    int b = 0;
    if(ctl_mode == ctl_mouse) {
      int dx, dy;
      ximg_mouse(ximg, &dx, &dy, &b);
      dprintf("dx=%d dy=%d b=%d\n",dx,dy,b);
      p.x = (f32)dx / w * s;
      p.y = (h-(f32)dy-1.0) / h * s;
      if(b & Button1Mask) {
	s += z;
      }
      if(b & Mod1Mask) { /* Button2Mask is not set ; debian-5.10 */
	x = p.x - (s/2);
	y = p.y - (s/2);
	printf("w=%d, h=%d, x=%f, y=%f, s=%f, z=%f\n", w, h, x, y, s, z);
      }
      if(b & Button3Mask) {
	s -= z;
      }
    } else {
      p.x = fis.ctl[0];
      p.y = fis.ctl[1];
      s = fis.ctl[2];
      img_i = fis.ctl[3];
    }
    renderC(img, h, w, x, y, s, &p, img_f[img_i]);
    ximg_blit(ximg, img);
    if(ctl_mode == ctl_mouse && b & ControlMask) {
      break;
    }
    frameCount += 1.0;
  }
  f64 end = current_time_as_utc_real();
  fprintf(stderr,"frameRate=%f (start=%f,end=%f,f=%f)\n",
	  frameCount/(end-start),
	  start,
	  end,
	  frameCount);
  ximg_close(ximg);
  return EXIT_SUCCESS;
}
