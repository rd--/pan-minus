all:
	echo "pan-minus"

clean:
	rm -Rf dist dist-newstyle *~
	(cd c ; make clean)

push-all:
	r.gitlab-push.sh pan-minus

push-tags:
	r.gitlab-push.sh pan-minus --tags

indent:
	fourmolu -i Graphics

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Graphics/Pan/Minus/*.hs
