module Graphics.Pan.Minus.Image where

import Graphics.Pan.Minus.Colour
import Graphics.Pan.Minus.Common
import Graphics.Pan.Minus.Point
import Graphics.Pan.Minus.Transform

infixr 3 `intersectR`, `diffR`
infixr 2 `unionR`

-- | An image is a function from a cartesian co-ordinate to a value
type Image c = P2 -> c

-- | A region is a boolean image
type Region = Image Bool

-- | An `ImageR' is a greyscale image
type ImageR = Image R

-- | An `ImageC' is a colour image
type ImageC = Image C4

type Time = R
type ImageB c = Time -> Image c
type ImageBC = ImageB C4

-- | A `Filter' is a function from one image to another
type Filter c = Image c -> Image c

type FilterC = Filter C4
type FilterB c = Image c -> ImageB c
type FilterBC = FilterB C4

-- | A `HyperFilter' is a function from one `Filter' to another
type HyperFilter c = Filter c -> Filter c

-- | Black and white colour `Image' of `Region'
bwIm :: Region -> ImageC
bwIm reg = cond reg blackI whiteI

-- | Blue and yellow colour `Image' of `Region'
byIm :: Region -> ImageC
byIm reg = cond reg blueI yellowI

-- | An empty colour image
emptyC :: p -> C4
emptyC = const invisible

cond :: (a -> Bool) -> (a -> b) -> (a -> b) -> (a -> b)
cond = lift3 ifF

over :: ImageC -> ImageC -> ImageC
over = lift2 overC

overs :: [ImageC] -> ImageC
overs = foldr over emptyC

fade :: ImageR -> ImageC -> ImageC
fade = lift2 fadeC

negI :: ImageC -> ImageC
negI = lift1 negC

lerpI :: ImageR -> ImageC -> ImageC -> ImageC
lerpI = lift3 lerpC

lightenI, darkenI :: ImageR -> FilterC
lightenI = lift2 lighten
darkenI = lift2 darken

-- | Extract alph channel of colour image
alphaI :: ImageC -> ImageR
alphaI = lift1 colorA

-- | Constant coloured images
redI, orangeI, greenI, yellowI, blueI, whiteI, blackI :: ImageC
redI = const red
orangeI = const orange
greenI = const green
yellowI = const yellow
blueI = const blue
whiteI = const white
blackI = const black

greyI :: R -> ImageC
greyI x = const (grey x)

imXf :: Transform -> Filter c
imXf xf im = im . xf

translate :: V2 -> Filter c
translate (V2 dx dy) = imXf (translateP (V2 (-dx) (-dy)))

scale :: V2 -> Filter c
scale (V2 sx sy) = imXf (scaleP (V2 (1 / sx) (1 / sy)))

rotate :: R -> Filter c
rotate ang = imXf (rotateP (-ang))

shear :: R -> Filter c
shear dxdy = imXf (shearP (-dxdy))

tile :: V2 -> Filter c
tile size = imXf (tileP size)

swirl :: R -> Filter c
swirl r = imXf (swirlP r)

cwave :: (R -> R) -> R -> Filter c
cwave f r = imXf (cwaveP f r)

uscale :: R -> Filter c
uscale s = scale (V2 s s)

hflip :: Filter c
hflip = scale (V2 (-1) 1)

vflip :: Filter c
vflip = scale (V2 1 (-1))

bilerpC :: C4 -> C4 -> C4 -> C4 -> ImageC
bilerpC ll lr ul ur (P2 dx dy) = lerpC dy (lerpC dx ll lr) (lerpC dx ul ur)

regionToImageC :: Region -> ImageC
regionToImageC reg = crop reg blackI

intersectR :: Region -> Region -> Region
intersectR = lift2 (&&)

unionR :: Region -> Region -> Region
unionR = lift2 (||)

xorR :: Region -> Region -> Region
xorR = lift2 (/=)

complementR :: Region -> Region
complementR = lift1 not

diffR :: Region -> Region -> Region
diffR r r' = r `intersectR` complementR r'

universeR :: Region
universeR = const True

-- | An empty `Region'
emptyR :: Region
emptyR = const False

-- | `Region' where x co-ordinate is positive
xPos :: Region
xPos (P2 x _) = x > 0

-- | `Region' where y co-ordinate is positive
yPos :: Region
yPos (P2 _ y) = y > 0

crop :: (p -> Bool) -> (p -> C4) -> (p -> C4)
crop reg im = cond reg im emptyC

-- | `Region' of unit disk
udisk :: Region
udisk p = distOsq p < 1

annulus :: R -> Region
annulus inner p =
  let r = distOsq p
  in inner * inner <= r && r < 1

annulus' :: R -> Region
annulus' inner = udisk `diffR` disk inner

disk :: R -> Region
disk r = uscale r udisk

diskIm :: R -> C4 -> ImageC
diskIm r c = crop (disk r) (const c)

box :: R -> R -> Region
box wid hgt (P2 x y) = abs x <= wid / 2 && abs y <= hgt / 2

withinI :: R -> ImageC -> ImageC -> Region
withinI epsilon = lift2 (withinC epsilon)

chromaReg :: R -> C4 -> ImageC -> Region
chromaReg epsilon bg im =
  let match col = withinI epsilon (const col) im
  in complementR (match invisible `unionR` match bg)

chromaKey :: R -> C4 -> FilterC
chromaKey epsilon bg im = crop (chromaReg epsilon bg im) im

about :: P2 -> HyperFilter c
about (P2 x y) imf = translate (V2 x y) . imf . translate (V2 (-x) (-y))

atScale :: V2 -> HyperFilter c
atScale v xf =
  let (V2 sx sy) = v
  in scale v . xf . scale (V2 (1 / sx) (1 / sy))

atUScale :: R -> HyperFilter c
atUScale s = atScale (V2 s s)

atRotate :: R -> HyperFilter c
atRotate ang xf = rotate ang . xf . rotate (-ang)

rippleRadius :: Int -> R -> Filter c
rippleRadius n s = imXf (rippleRadiusP n (-s))

pinwheel :: R -> Int -> R -> Filter c
pinwheel r n s = swirl r . rippleRadius n s

radReg :: Int -> Region
radReg arms =
  let test a = even (floor (a * i2f arms / pi) :: Int)
  in test . pp2_theta . toPolar

radCrop :: Int -> FilterC
radCrop = crop . radReg

wedges :: Int -> R -> Region
wedges arms frac =
  let f a =
        let a' = a + ifF (a < 0) (2 * pi) 0
        in fracPart (a' * i2f arms / (2 * pi)) <= frac
  in rotate (pi / 2 + pi * (1 - frac) / i2f arms) (f . pp2_theta . toPolar)

iswirl :: Bool -> R -> R -> Filter c
iswirl inside rad sw = imXf (iswirling inside rad (-sw))

radInvert :: Filter c
radInvert im = im . radInvertP

lightnessI, darknessI :: ImageC -> ImageR
lightnessI = lift1 lightness
darknessI = lift1 darkness

transpLightI :: ImageC -> ImageC
transpLightI im = fade (darknessI im) im

legitI :: ImageC -> Region
legitI = lift1 legitC

type ImageBXf c = ImageB c -> ImageB c

gTile :: (R, R) -> ((Int, Int) -> R) -> ImageBXf c
gTile (w, h) dt imb t (P2 x y) =
  let z `divM` lim =
        let l2 = lim / 2
            z' = z + l2
            tnum = floor (z' / lim)
        in (tnum, z - i2f tnum * lim)
      (tx, dx) = x `divM` w
      (ty, dy) = y `divM` h
  in imb (t + dt (tx, ty)) (P2 dx dy)
