module Graphics.Pan.Minus.Mandelbrot where

import Data.Complex {- base -}

import Graphics.Pan.Minus.Image
import Graphics.Pan.Minus.Point

-- | Mandelbrot quadratic recurrence equation z_(n+1)==z_n^2+C
mset :: (RealFloat t) => Complex t -> Complex t -> Complex t
mset z c = z * z + c

modulus :: (RealFloat t) => Complex t -> t
modulus (x :+ y) = sqrt (x * x + y * y)

mset_b :: R -> Int -> Complex R -> Bool
mset_b limit n c =
  let loop i z =
        if i == n
          then True
          else
            if magnitude z > limit
              then False
              else loop (i + 1) (mset z c)
  in loop 0 (mset c c)

mset_f :: R -> Int -> Complex R -> R
mset_f limit n c =
  let loop i z =
        if i == n
          then 0
          else
            if magnitude z > limit
              then 1 - (fromIntegral i / fromIntegral n)
              else loop (i + 1) (mset z c)
  in loop 0 (mset c c)

mset_img_b :: R -> Int -> Image Bool
mset_img_b limit n (P2 x y) = mset_b limit n (x :+ y)

mset_img_f :: R -> Int -> Image R
mset_img_f limit n (P2 x y) = mset_f limit n (x :+ y)
