module Graphics.Pan.Minus.Colour where

import Graphics.Pan.Minus.Point (R)

-- | Real valued Rgba colour.  Alpha of 0 indicates transparent, 1 indicates opaque.
data C4 = C4
  { colorR :: !R
  , colorG :: !R
  , colorB :: !R
  , colorA :: !R
  }

-- | Reverse order constructor
bgra :: R -> R -> R -> R -> C4
bgra b g r a = C4 r b g a

-- | Reverse order opaque constructor
bgr :: R -> R -> R -> C4
bgr b g r = C4 r b g 1

-- | Named colours
red, orange, green, yellow, blue, white, black, invisible :: C4
red = bgr 0 0 1
orange = bgr 0 0.65 1
green = bgr 0 1 0
yellow = bgr 0 1 1
blue = bgr 1 0 0
white = bgr 1 1 1
black = bgr 0 0 0
invisible = bgra 0 0 0 0

-- | Grey-scale constructor
grey :: R -> C4
grey x = bgr x x x

lighten :: R -> C4 -> C4
lighten x c = lerpC x c white

darken :: R -> C4 -> C4
darken x c = lerpC x c black

withinC :: R -> C4 -> C4 -> Bool
withinC epsilon c c' = c `distSqC` c' < epsilon ^ (2 :: Int)

lightness :: C4 -> R
lightness (C4 r g b _) = (b + g + r) / 3

darkness :: C4 -> R
darkness c = 1 - lightness c

luminance :: C4 -> R
luminance (C4 r g b _) = (b + g + r + 2) / 3

legitC :: C4 -> Bool
legitC (C4 r g b a) =
  let tst x = x <= a
  in and [tst x | x <- [b, g, r]]

isPreMultiplied :: C4 -> Bool
isPreMultiplied (C4 r g b a) = a >= b && a >= g && a >= r

dotC :: C4 -> C4 -> R
dotC c1 c2 =
  let (C4 r1 g1 b1 a1) = c1
      (C4 r2 g2 b2 a2) = c2
  in a1 * a2 + r1 * r2 + g1 * g2 + b1 * b2

distSqC :: C4 -> C4 -> R
distSqC c1 c2 =
  let d = pointwiseC (-) c1 c2
  in dotC d d

distC :: C4 -> C4 -> R
distC c c' = sqrt (distSqC c c')

negC :: C4 -> C4
negC (C4 r g b a) = C4 (a - r) (a - g) (a - b) a

pointwiseC :: (R -> R -> R) -> C4 -> C4 -> C4
pointwiseC f c1 c2 =
  let (C4 r1 g1 b1 a1) = c1
      (C4 r2 g2 b2 a2) = c2
  in C4 (f r1 r2) (f g1 g2) (f b1 b2) (f a1 a2)

lerpC :: R -> C4 -> C4 -> C4
lerpC w =
  let h x1 x2 = (w * x1) + ((1 - w) * x2)
  in pointwiseC h

fadeC :: R -> C4 -> C4
fadeC f (C4 r g b a) =
  let h n = n * f
  in C4 (h r) (h g) (h b) (h a)

overC :: C4 -> C4 -> C4
overC c1 =
  let a1 = colorA c1
      f x1 x2 = x1 + ((1 - a1) * x2)
  in pointwiseC f c1
