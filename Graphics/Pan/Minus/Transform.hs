module Graphics.Pan.Minus.Transform where

import Graphics.Pan.Minus.Common
import Graphics.Pan.Minus.Point

-- | A `Transform' is a function from one cartesian co-ordinate to another
type Transform = P2 -> P2

-- | A `PTransform' is a function from one polar co-ordinate to another
type PTransform = PP2 -> PP2

-- | Construct a rotation `Transform' of `ang' degrees (radians)
rotateP :: R -> Transform
rotateP ang (P2 x y) =
  let c = cos ang
      s = sin ang
  in P2 (x * c - y * s) (y * c + x * s)

-- | Construct a transalation `Transform'
translateP :: V2 -> Transform
translateP (V2 dx dy) (P2 x y) = P2 (x + dx) (y + dy)

-- | Construct a scaling `Transform'
scaleP :: V2 -> Transform
scaleP (V2 sx sy) (P2 x y) = P2 (sx * x) (sy * y)

-- | Construct a scaling `Transform' equal in both dimensions
uscaleP :: R -> Transform
uscaleP s = scaleP (V2 s s)

shearP :: R -> Transform
shearP dxdy (P2 x y) = P2 (x + dxdy * y) y

tileP :: V2 -> Transform
tileP (V2 w h) (P2 x y) = P2 (wrap' w x) (wrap' h y)

polarXf :: PTransform -> Transform
polarXf xf = fromPolar . xf . toPolar

swirlP :: R -> Transform
swirlP r p = rotateP (distO p * (2 * pi / r)) p

cwaveP :: (R -> R) -> R -> Transform
cwaveP f r p = uscaleP (f (distO p * (2 * pi / r))) p

wrap :: R -> R -> R
wrap w x = w * fracPart (x / w)

wrap' :: R -> R -> R
wrap' w x =
  let w2 = w / 2
  in wrap w (x + w2) - w2

xfAbout :: P2 -> Transform -> Transform
xfAbout (P2 x y) xf = translateP (V2 x y) . xf . translateP (V2 (-x) (-y))

-- | A `Transform1' is a function from one Real to another
type Transform1 = R -> R

radialXf :: Transform1 -> Transform
radialXf f = polarXf (\(PP2 r a) -> PP2 (f r) a)

angularXf :: Transform1 -> Transform
angularXf f = polarXf (\(PP2 r a) -> PP2 r (f a))

radialScaleXf :: Transform1 -> Transform
radialScaleXf f = radialXf (\r -> r * f r)

rotateAbout :: P2 -> R -> Transform
rotateAbout p ang = xfAbout p (rotateP ang)

scaleAbout :: P2 -> V2 -> Transform
scaleAbout p sc = xfAbout p (scaleP sc)

uscaleAbout :: P2 -> R -> Transform
uscaleAbout p sc = scaleAbout p (V2 sc sc)

rippleRadiusP' :: Int -> R -> Transform
rippleRadiusP' n s =
  let xf (PP2 rho theta) = PP2 (rho + s * sin (i2f n * theta)) theta
  in polarXf xf

rippleRadiusP :: Int -> R -> Transform
rippleRadiusP n s =
  let xf (PP2 rho theta) = PP2 (rho * (1 + s * sin (i2f n * theta))) theta
  in polarXf xf

iswirling :: Bool -> R -> R -> Transform
iswirling inside rad sw p =
  let d = rad - distO p
      a = if (mbNot (d >= 0)) then 0 else (2 * pi * sw * d / rad)
      mbNot = if inside then not else id
  in rotateP a p

radInvertP :: Transform
radInvertP = polarXf (\(PP2 r th) -> PP2 (1 / r) th)
