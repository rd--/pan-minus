module Graphics.Pan.Minus.Bitmap where

import Graphics.Pan.Minus.Colour
import Graphics.Pan.Minus.Common
import Graphics.Pan.Minus.Image
import Graphics.Pan.Minus.Point

under :: R -> Int -> Bool
under z m =
  let z' = floor z
  in 0 <= z' && z' <= m - 1

inBounds :: Int -> Int -> Region
inBounds w h (P2 x y) = x `under` w && y `under` h

pixelize :: (Int, Int) -> Image a -> Array2 a
pixelize (w, h) img =
  let t_img =
        let v = i2f_V2 (w `div` 2, h `div` 2)
        in translate v img
  in Array2 w h (t_img . i2f_P2)

data Array2 a = Array2 Int Int ((Int, Int) -> a)

bilerpCArray2 :: ((Int, Int) -> C4) -> ImageC
bilerpCArray2 sub (P2 x y) =
  let i = floor x
      j = floor y
      dx = x - i2f i
      dy = y - i2f j
  in bilerpC (sub (i, j)) (sub (i + 1, j)) (sub (i, j + 1)) (sub (i + 1, j + 1)) (P2 dx dy)

truncateCArray2 :: ((Int, Int) -> C4) -> ImageC
truncateCArray2 sub (P2 x y) = sub (floor x, floor y)

reconstruct :: (((Int, Int) -> C4) -> ImageC) -> Array2 C4 -> ImageC
reconstruct f (Array2 w h sub) =
  let cropped = crop (inBounds (w - 1) (h - 1)) (f sub)
  in translate (V2 (-i2f (w `div` 2)) (-i2f (h `div` 2))) cropped

reconstructB :: Array2 C4 -> ImageC
reconstructB = reconstruct bilerpCArray2

reconstructT :: Array2 C4 -> ImageC
reconstructT = reconstruct truncateCArray2
