module Graphics.Pan.Minus.Common where

import Graphics.Pan.Minus.Point

-- | Square root of sum of squares.
hypotenuse :: R -> R -> R
hypotenuse x y = sqrt (x * x + y * y)

lift1 :: (a -> b) -> (p -> a) -> (p -> b)
lift1 h f1 x = h (f1 x)

lift2 :: (a -> b -> c) -> (p -> a) -> (p -> b) -> (p -> c)
lift2 h f1 f2 x = h (f1 x) (f2 x)

lift3 :: (a -> b -> c -> d) -> (p -> a) -> (p -> b) -> (p -> c) -> (p -> d)
lift3 h f1 f2 f3 x = h (f1 x) (f2 x) (f3 x)

-- | If as function, for lifting
ifF :: Bool -> a -> a -> a
ifF a b c = if a then b else c

-- | Int to Real
i2f :: Int -> R
i2f i = fromIntegral i

-- | Real to Int
floorI :: R -> Int
floorI = floor

-- | Int pair to vector
i2f_V2 :: (Int, Int) -> V2
i2f_V2 (i, j) = V2 (i2f i) (i2f j)

-- | Int pair to cartesian point
i2f_P2 :: (Int, Int) -> P2
i2f_P2 (i, j) = P2 (i2f i) (i2f j)

-- | Fractional part of Real number
fracPart :: R -> R
fracPart x = x - i2f (floor x)
