module Graphics.Pan.Minus.Render.Gtk (displayImageC) where

-- import Control.Monad
import qualified Data.Array.Base as A {- array -}
-- import Data.Array.MArray

import Data.Word {- base -}

import qualified Graphics.UI.Gtk as G {- gtk -}
import qualified Graphics.UI.Gtk.Gdk.Events as E
import qualified Graphics.UI.Gtk.Gdk.GC as G

import Graphics.Pan.Minus.Colour
import Graphics.Pan.Minus.Image
import Graphics.Pan.Minus.Point

-- mb = make byte
mb :: R -> Word8
mb c = fromIntegral (floor (c * 256.0) :: Int)

doFromTo :: Int -> Int -> (Int -> R -> IO ()) -> IO ()
doFromTo from to action =
  let z = 1.0 / (fromIntegral (to - from - 1))
      loop n m
        | n > to = return ()
        | otherwise = do
            action n m
            loop (n + 1) (m + z)
  in loop from 0.0

-- d = data, xe = x extent, ye = y extent, f = image, ro = row offset,
renderImage :: (G.PixbufData Int Word8) -> Int -> Int -> ImageC -> IO ()
renderImage d xe ye f = do
  let ro = xe * 3
      inner :: Int -> R -> Int -> R -> IO ()
      inner yi yr xi xr = do
        let k = xi * 3 + yi * ro
            (C4 r g b _) = f (P2 xr yr)
        A.unsafeWrite d k (mb r)
        A.unsafeWrite d (k + 1) (mb g)
        A.unsafeWrite d (k + 2) (mb b)
      outer yi yr = doFromTo 0 xe (inner yi yr)
  doFromTo 0 ye outer

displayImageC :: ImageC -> IO ()
displayImageC f = do
  _ <- G.initGUI
  window <- G.windowNew
  canvas <- G.drawingAreaNew
  let w = 512
      h = 256
  G.windowSetResizable window False
  G.widgetSetSizeRequest window w h
  p <- G.pixbufNew G.ColorspaceRgb False 8 w h
  d <- (G.pixbufGetPixels p :: IO (G.PixbufData Int Word8))
  -- r <- G.pixbufGetRowstride p
  -- c <- G.pixbufGetNChannels p
  renderImage d w h f
  _ <- G.onDestroy window G.mainQuit
  _ <- G.onExpose canvas (updateCanvas canvas p)
  G.set window [G.containerChild G.:= canvas]
  G.widgetShowAll window
  G.mainGUI

updateCanvas :: G.DrawingArea -> G.Pixbuf -> E.Event -> IO Bool
updateCanvas canvas pb E.Expose {E.eventRegion = region} = do
  win <- G.widgetGetDrawWindow canvas
  gc <- G.gcNew win
  width <- G.pixbufGetWidth pb
  height <- G.pixbufGetHeight pb
  pbregion <- G.regionRectangle (G.Rectangle 0 0 width height)
  G.regionIntersect region pbregion
  rects <- G.regionGetRectangles region
  (flip mapM_) rects $ \(G.Rectangle x y w h) -> do
    G.drawPixbuf win gc pb x y x y w h G.RgbDitherNone 0 0
  return True
updateCanvas _ _ _ = error "updateCanvas"
