module Graphics.Pan.Minus.Render.Ppm where

import qualified Codec.Image.PPM as P {- ppm -}

import Graphics.Pan.Minus.Colour
import Graphics.Pan.Minus.Image
import Graphics.Pan.Minus.Point

-- | Scale [0,1] real to [0,255] integer
to_byte :: R -> Int
to_byte = floor . (* 255)

-- | Down-sample real colour (C4) to 8-bit
to_pixel :: C4 -> (Int, Int, Int)
to_pixel (C4 r g b _) = (to_byte r, to_byte g, to_byte b)

-- | Interpolate [-1,1] to `i' places.
interp :: Int -> [Double]
interp i =
  let i' = fromIntegral i
      f j = (j / (i' / 2)) - 1
  in [f n | n <- [0 .. i']]

{- | Write `C4' image to PPM file.
nm = file-name, w = width, h = height, f = image
-}
writeImageC :: String -> Int -> Int -> ImageC -> IO ()
writeImageC nm w h f =
  let row y = [(to_pixel . f) (P2 x y) | x <- interp w]
  in writeFile nm (P.ppm (reverse (map row (interp h))))
