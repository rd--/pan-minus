-- | Conal Elliott "Functional Image Synthesis", Microsoft Research, 2000
module Graphics.Pan.Minus.Fis where

import Graphics.Pan.Minus.Colour
import Graphics.Pan.Minus.Common
import Graphics.Pan.Minus.Image
import Graphics.Pan.Minus.Point
import Graphics.Pan.Minus.Transform

-- | Fig. 1
vstrip :: Region
vstrip (P2 x _) = (abs x) <= (1 / 2)

-- | Fig. 2
checker :: Region
checker (P2 x y) = even (floorI x + floorI y)

-- | Fig. 3
altRings :: Region
altRings = even . floorI . distO

-- | Fig. 4
polarChecker :: Int -> Region
polarChecker n =
  let n' = fromIntegral n
      sc (PP2 r a) = P2 r (a * n' / pi)
  in checker . sc . toPolar

-- | Fig. 5
wavDist :: ImageR
wavDist p = (1 + cos (pi * distO p)) / 2

-- | Fig. 6
bilerpBRBW :: ImageC
bilerpBRBW = bilerpC black red blue white

-- | Fig. 8
ybRings :: ImageC
ybRings = lerpI wavDist yellowI blueI

cropRadius :: R -> Filter C4
cropRadius = crop . disk

cropRadiusU :: Filter C4
cropRadiusU = cropRadius 1

-- | Fig. 13
wedgeAnnulus :: R -> Int -> Region
wedgeAnnulus inner n = annulus inner `intersectR` radReg n

-- | Fig. 15
shiftXor :: R -> Filter Bool
shiftXor r reg =
  let reg' d = translate (V2 d 0) reg
  in xorR (reg' r) (reg' (-r))

xorRs :: [Region] -> Region
xorRs = foldr xorR emptyR

-- | Fig. 16
xorgon :: R -> R -> Region -> Region
xorgon n r reg =
  let rf i =
        let a = (i * 2 * pi) / n
            (P2 dx dy) = fromPolar (PP2 r a)
        in translate (V2 dx dy) reg
  in xorRs (map rf [0 .. n - 1])

-- | Fig. 7.24
circleLimit :: R -> FilterC
circleLimit radius im =
  let xf (PP2 rho theta) = PP2 (radius * (rho / (radius - rho))) theta
  in cropRadius radius (im . polarXf xf)

checkerBoard :: R -> Image c -> Image c -> Image c
checkerBoard sqSize = cond (uscale sqSize checker)
