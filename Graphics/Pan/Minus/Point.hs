module Graphics.Pan.Minus.Point where

-- | R = Real
type R = Double

-- | Cartesian co-ordinate (two dimensional)
data P2 = P2 {p2_x :: !R, p2_y :: !R}

-- | Polar co-ordinate (two dimensional)
data PP2 = PP2 {pp2_rho :: !R, pp2_theta :: !R}

-- | Cartesian vector (two dimensional)
data V2 = V2 {v2_dx :: !R, v2_dy :: !R}

-- | Square of distance to origin
distOsq :: P2 -> R
distOsq (P2 x y) = x ^ (2 :: Int) + y ^ (2 :: Int)

-- | Distance to origin.
distO :: P2 -> R
distO = sqrt . distOsq

-- | Angle from origin.
angleO :: P2 -> R
angleO (P2 x y) = atan2 y x

-- | Translate polar to cartesian.
fromPolar :: PP2 -> P2
fromPolar (PP2 rho theta) = P2 (rho * cos theta) (rho * sin theta)

-- | Translate cartesian to polar.
toPolar :: P2 -> PP2
toPolar p = PP2 (distO p) (angleO p)
